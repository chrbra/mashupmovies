package project.repository;

import org.springframework.stereotype.Repository;
import project.model.Movie;
import project.model.Review;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class MovieRepositoryMockup {
    private final List<Movie> movies;

    public MovieRepositoryMockup() {
        movies = new ArrayList<>() {{
            add(new Movie(590706, "Jiu Jitsu", Collections.singletonList(new Review(1, "Test 1",
                    "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et", "Chris")
                    )));
        }};
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public Movie getMovieById(int id) {
        for (Movie movie : movies) {
            if (movie.getId() == id) {
                return movie;
            }
        }
        return null;
    }

    public boolean deleteById(int id) {
        return movies.remove(getMovieById(id));
    }

    public Movie saveMovie(Movie movie) {
        for (int i = 0; i < movies.size(); i++) {
            if (movies.get(i).getId().equals(movie.getId())) {
                movies.set(i, movie);
                return movie;
            }
        }
        movies.add(movie);
        return movie;
    }
}
