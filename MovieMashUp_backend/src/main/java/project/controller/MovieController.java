package project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import project.model.Movie;
import project.repository.MovieRepositoryMockup;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class MovieController {
    MovieRepositoryMockup movieRepositoryMockup;

    @Autowired
    public MovieController(MovieRepositoryMockup movieRepositoryMockup) {
        this.movieRepositoryMockup = movieRepositoryMockup;
    }

    @GetMapping("/movies")
    public List<Movie> getMovies() {
        return movieRepositoryMockup.getMovies();
    }

    @PostMapping("/movies")
    public Movie saveMovie(@RequestBody Movie movie) {
        return movieRepositoryMockup.saveMovie(movie);
    }




    @DeleteMapping("/movies/{id}")
    public ResponseEntity<?> deleteMovie(@PathVariable int id) {
        boolean result = movieRepositoryMockup.deleteById(id);
        if (result) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
