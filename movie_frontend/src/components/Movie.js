import React, {useState} from 'react';
import '../css/movie.css';

const Movie = ({movie, movies, setMoviesWithReviews}) => {

    const [isFormVisible, setIsFormVisible] = useState(false);
    const [showButton, setShowButton] = useState(true);
    const imgUrl = "http://image.tmdb.org/t/p/w185/"

    const createReview = (e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        let review = {
            "title": formData.get('title'),
            "description": formData.get('description'),
            "initials": formData.get('initials')
        }
        if (movie.reviews) {
            movie.reviews = [...movie.reviews, review]
        } else {
            movie.reviews = [review];
        }
        console.log(movie.reviews);

        fetch(`http://localhost:8080/movies`)
            .then(resp => resp.json())
            .then(moviesFromBackend => {
                //iterieren durch die erhaltenen Movies
                moviesFromBackend.forEach(movieFromBackend => {
                    console.log(movieFromBackend);
                    //überprüfen ob dieser Movie schon existiert
                    if (movieFromBackend.id === movie.id) {
                        movieFromBackend.reviews.push(review);

                        fetch(`http://localhost:8080/movies`, {
                            method: 'POST',
                            headers: {
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify(movieFromBackend)
                        }).then(resp => resp.json())
                            .then(movie => {
                                setMoviesWithReviews([...movies, movie])
                            })
                        return null;
                    }
                });
                    fetch(`http://localhost:8080/movies`, {
                        method: 'POST',
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify(movie)
                    }).then(resp => resp.json())
                        .then(movie => {
                            setMoviesWithReviews([...movies, movie])
                        })

            });

    }

    const openReviewForm = () => {
        setShowButton(!showButton)
        setIsFormVisible(!isFormVisible);
    }

    const closeReview = () => {
        setShowButton(true)
        setIsFormVisible(false)
    }

    return (
        <div className="container-1">
            <div className="container-2">
                <div className="image-container">
                    <img className="image" src={imgUrl + movie.poster_path}/>
                </div>
                <div className="content-container">
                    <div className="title">
                        <h2>{movie.title}</h2>
                    </div>
                    <div className="movie-details">
                        <div className="characteristics">
                            Original Title:

                            Release Date:
                            <br/>
                            Average Voting:
                            <br/>
                            Overview:
                        </div>
                        <div className="details">
                            {movie.original_title}
                            <br/>
                            {movie.release_date}
                            <br/>
                            {movie.vote_average} ({movie.vote_count})
                            <br/>
                            {movie.overview}
                        </div>
                    </div>


                    <br/>
                    {showButton ?
                        <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                            <button className="reviewButton" onClick={openReviewForm}>
                                Write a review!
                            </button>
                        </div>
                        :
                        null
                    }
                </div>
            </div>
            <div className="review-form" style={{display: isFormVisible ? 'block' : 'none'}}>
                <form onSubmit={createReview}>
                    <p>
                        <input name="title" id="title" type="text" placeholder="Title" required/>
                    </p>
                    <p>
                        <textarea name="description" id="description" placeholder="Review" rows="4" cols="70" required/>
                    </p>
                    <input name="initials" id="initials" type="text" placeholder="Initials"/>
                    <div className="review-buttons">
                        <button className="review-cancelButton" type="button" onClick={closeReview}>Cancel</button>
                        <button type="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
    );
}

export default Movie;
