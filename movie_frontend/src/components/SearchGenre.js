import React, { useState } from 'react';
import Dropdown from "react-bootstrap/Dropdown";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/searchGenre.css'

 const SearchGenre = ({genres, setMovies}) => {
     const [dropDownValue, setDropDownValue] = useState("Select a genre");
     const [id, setId] = useState(0);
     const API_KEY = "8448e2d8bdf9d4e44a322a165c5438a1";

     const search = (e) => {
         e.preventDefault();
         fetch(`https://api.themoviedb.org/3/discover/movie?api_key=${API_KEY}&language=en-US&sort_by=popularity.desc&page=1&with_genres=${id}`)
             .then(resp => resp.json())
             .then(data => setMovies(data.results));
     }

     return (
        <div className="container-genre">
            <div className="dropdown-genre">
                <Dropdown >
                    <Dropdown.Toggle variant="success" id="dropdown">
                        {dropDownValue}
                    </Dropdown.Toggle>

                    <Dropdown.Menu>
                        {genres.map(genre => (
                            <Dropdown.Item key={genre.id} onClick={() => {
                                setDropDownValue(genre.name);
                                setId(genre.id);
                            }}>{genre.name}</Dropdown.Item>
                        ))}
                    </Dropdown.Menu>
                </Dropdown>
            </div>

            <button onClick={search} disabled={dropDownValue === "Select a genre"}>
                Search
            </button>
        </div>
    )
}

export default SearchGenre;