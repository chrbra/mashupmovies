import React, {useEffect, useState} from 'react';
import Review from "./Review";
import CardColumns from 'react-bootstrap/CardColumns'

const MovieReviews = ({movie}) => {

    const [areReviewsVisible, setAreReviewsVisible] = useState(false);
    const imgUrl = "http://image.tmdb.org/t/p/w185/"

    useEffect(() => {
        // console.log({movie})
    }, [movie])

    return (
        <div className="container-1">
            <div className="container-2">
                <div className="image-container">
                    <img className="image" src={imgUrl + movie.poster_path}/>
                </div>
                <div className="content-container">
                    <div className="title">
                        <h2>{movie.title}</h2>
                    </div>
                    <div className="movie-details">
                        <div className="characteristics">
                            Original Title:
                            Release Date:
                            <br/>
                            Average Voting:
                            <br/>
                            Overview:
                        </div>
                        <div className="details">
                            {movie.original_title}
                            <br/>
                            {movie.release_date}
                            <br/>
                            {movie.vote_average} ({movie.vote_count})
                            <br/>
                            {movie.overview}
                        </div>
                    </div>
                    <br/>
                    <div style={{display: 'flex', justifyContent: 'flex-end'}}>
                        <button className="reviewButton" onClick={() => setAreReviewsVisible(!areReviewsVisible)}>
                            {areReviewsVisible ? "Hide" : "Show reviews a review!"}
                        </button>
                    </div>
                </div>
            </div>
            {areReviewsVisible ?
                <div className="review-form">
                    <CardColumns>
                        {movie.reviews.map((review, index) => {
                            return (
                                <Review key={index} review={review}/>
                            );
                        })}
                    </CardColumns>
                </div>
                : null}
        </div>
    );
}

export default MovieReviews;
