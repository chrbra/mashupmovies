import React from 'react';
import Card from 'react-bootstrap/Card';
// import '../css/movie.css';

const Review = ({review}) => {
    return (

        <div className="container-1">
            <Card style={{ width: '18rem' }}>
                <Card.Body>
                    <Card.Title>{review.title}</Card.Title>
                    <Card.Text>
                        {review.description}
                    </Card.Text>
                    <Card.Footer>
                        {review.initials}
                    </Card.Footer>
                    {/*<Button variant="primary">Go somewhere</Button>*/}
                </Card.Body>
            </Card>
        </div>
    );
}

export default Review;
