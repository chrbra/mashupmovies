import React, {useEffect, useState} from 'react';
import Home from './pages/Home';
import {Route, Switch} from "react-router-dom";
import Reviews from "./pages/Reviews";
import About from "./pages/About";
import Header from "./components/Header";
import './css/app.css'

export default function App() {

    const API_KEY = "8448e2d8bdf9d4e44a322a165c5438a1"
    const [moviesWithReviews, setMoviesWithReviews] = useState([]);
    let [pictureURL, setPictureURL] = useState("");

    useEffect(() => {
        let movies = [];
        fetch(`http://localhost:8080/movies`)
            .then(resp => resp.json())
            .then(moviesFromBackend => {
                console.log({moviesFromBackend})
                moviesFromBackend.forEach(movieFromBackend => {
                    fetch(`https://api.themoviedb.org/3/movie/${movieFromBackend.id}?api_key=${API_KEY}`)
                        .then(resp => resp.json())
                        .then(newMovie => movies.push({...newMovie, reviews: movieFromBackend.reviews}))
                })
            })

        setTimeout(() => {
            setMoviesWithReviews(movies);
        }, 1000)
    }, [])

    useEffect(() => {
        fetch("https://api.unsplash.com//photos/GF8VvBgcJ4o/?client_id=VmlCdlHb-lYXBdcCP6-cUdFt18SPXc-rCanrVJ9oKt4&query=cinema&orientation=landscape")
            .then(response => response.json())
            .then(picture =>
                setPictureURL(picture.urls.regular)
            );
    }, []);

    useEffect(() => {
        document.body.background = pictureURL;
    }, [pictureURL]);

    useEffect(() => {
        console.log({moviesWithReviews})
    }, [moviesWithReviews]);

    return (
        <>
            <Header/>
            <Switch>
                <Route exact={true} path="/">
                    <Home setMoviesWithReviews={setMoviesWithReviews}/>
                </Route>
                <Route exact={true} path="/review">
                    <Reviews moviesWithReviews={moviesWithReviews}/>
                </Route>
                <Route exact={true} path="/about">
                    <About/>
                </Route>
            </Switch>
        </>
    );
}