import React from 'react';
import MovieReviews from "../components/MovieReviews";
import '../css/reviews.css';

const Reviews = ({moviesWithReviews}) => {

    return (
        <div className="container">
            {moviesWithReviews.map(movie => {
                return <MovieReviews key={movie.id} movie={movie}/>
            })}
        </div>
    );
}

export default Reviews;



