import React, {useEffect, useState} from 'react';
import SearchGenre from "../components/SearchGenre";
import Movie from "../components/Movie";


export default function Home({setMoviesWithReviews}) {

    const API_KEY = "8448e2d8bdf9d4e44a322a165c5438a1";
    const [genres, setGenres] = useState([]);
    const [movies, setMovies] = useState([]);

    useEffect(() => {
        loadGenres();
    }, [])

    function loadGenres() {
        fetch(`https://api.themoviedb.org/3/genre/movie/list?api_key=${API_KEY}&language=en-En`)
            .then(response => response.json())
            .then(genres => {
                    setGenres(genres.genres);
                }
            )
    }


    return (
        <div className="container">
            <SearchGenre genres={genres} setMovies={setMovies}/>
            {movies.length !== 0 ?
                <div className="movies">
                    {movies.map(movie => {
                        return (
                            <Movie setMoviesWithReviews={setMoviesWithReviews} key={movie.id} movie={movie}
                                   movies={movies}/>
                        )
                    })}
                </div>
                :
                null
            }

        </div>
    )
}



